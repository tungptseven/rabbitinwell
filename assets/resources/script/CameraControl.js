// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        Player_Node: cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    update(dt) {
        let target_position = this.Player_Node.getPosition()

        // target_position.y = cc.misc.clampf(target_position.y, 0, 720)

        target_position.x = cc.misc.clampf(target_position.x, 0, 0)

        let current_position = this.node.getPosition()

        current_position.lerp(target_position, 0.1, current_position)

        // current_position.y = cc.misc.clampf(target_position.y, 0, 220)

        this.node.setPosition(current_position)
    },
});
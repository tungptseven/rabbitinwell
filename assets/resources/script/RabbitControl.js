// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {},

    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onPressed, this)
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onReleased, this)


        this.Rigid_Body = this.node.getComponent(cc.RigidBody)
        this.distanceJoint = this.getComponent(cc.DistanceJoint)

        this.Direction = 0
        this.Velocity_Max_X = 400
        this.Walk_Force = 15000
        this.Jump_Force = 500000
        this.On_The_Ground = false
    },

    onPressed(event) {
        let key_code = event.keyCode

        switch (key_code) {
            case cc.macro.KEY.left:
                this.Direction = -1
                break

            case cc.macro.KEY.right:
                this.Direction = 1
                break

            case cc.macro.KEY.up:
                if (this.On_The_Ground) {
                    if (this.distanceJoint.enabled) {
                        // this.distanceJoint.connectedBody = null
                        // this.distanceJoint.anchor = cc.v2(0, 0)
                        // this.distanceJoint.connectedAnchor = cc.v2(0, 0)
                        // this.distanceJoint.distance = 0
                        this.distanceJoint.onDestroy()

                        // this.distanceJoint.connectedBody = null
                    }
                    this.Rigid_Body.applyForceToCenter(cc.v2(0, this.Jump_Force), true)
                    this.On_The_Ground = false
                }
                break
        }
    },

    onReleased(event) {
        let key_code = event.keyCode

        switch (key_code) {
            case cc.macro.KEY.left:
            case cc.macro.KEY.right:
                this.Direction = 0
                break
        }
    },

    onBeginContact(contact, selfCollider, otherCollider) {

        if (selfCollider.tag === 2) {
            this.On_The_Ground = true

            if (otherCollider.node.name == 'Obstacle') {
                let rabbit = selfCollider.body;
                let targetBody = otherCollider.body;

                // if (joint.enabled) {
                //     joint.enabled = false;
                //     return;
                // }

                this.distanceJoint.connectedBody = targetBody;
                this.distanceJoint.anchor = cc.v2(0, -50)
                this.distanceJoint.connectedAnchor = cc.v2(0, 25)
                this.distanceJoint.distance = 0
                this.distanceJoint.onEnable()
            }


        }
    },

    update(dt) {
        if ((this.Direction > 0 && this.Rigid_Body.linearVelocity.x < this.Velocity_Max_X) ||
            (this.Direction < 0 && this.Rigid_Body.linearVelocity.x > -this.Velocity_Max_X)
        ) {
            this.Rigid_Body.applyForceToCenter(cc.v2(this.Direction * this.Walk_Force, 0), true)
        }
    },
});
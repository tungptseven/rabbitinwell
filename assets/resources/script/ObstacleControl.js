// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    },

    start() {
        // cc.log(this.node)
        var minTime = 5
        var maxTime = 3
        let moveDuration = minTime + Math.random() * (maxTime - minTime)
        var moveLeft = cc.moveTo(moveDuration, cc.v2(-500, this.node.y));
        // jump down
        var moveRight = cc.moveTo(moveDuration, cc.v2(500, this.node.y));
        // repeat
        var moveAction = cc.repeatForever(cc.sequence(moveLeft, moveRight));

        this.node.runAction(moveAction);
    },

    // update (dt) {},
});
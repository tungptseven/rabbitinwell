// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        obstaclePrefab: cc.Prefab
    },


    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        let physics_manager = cc.director.getPhysicsManager()
        physics_manager.enabled = true
        physics_manager.gravity = cc.v2(0, -2000)
        physics_manager.debugDrawFlags = true

        let y = -100
        for (let i = 0; i < 10; i++) {
            this.onSpawn(y)
            y += 200
        }

    },

    start() {

    },

    onSpawn(y) {
        let obstacle = cc.instantiate(this.obstaclePrefab)
        this.node.getChildByName('Obstacle').addChild(obstacle)

        var minX = -500
        var maxX = 500
        obstacle.y = y
        // obstacle.x = 0
        obstacle.x = minX + Math.random() * (maxX - minX)

        // let actionBy = cc.moveTo(2, cc.v2(-1400, dart.y))
        // let destruction = cc.callFunc(() => {
        //     dart.destroy()
        // }, this)
        // let sequence = cc.sequence(actionBy, destruction)
        // dart.runAction(sequence)
    },

    // update (dt) {},
});